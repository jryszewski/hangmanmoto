import org.hibernate.query.criteria.internal.expression.function.CurrentDateFunction;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

class Hangman {

    static ArrayList<String> cities = new ArrayList<>();
    static ArrayList<Character> notInWordList = new ArrayList<>();
    static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm");
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int guesingTries = 0;
        int n = 1;
        while (n == 1) {
            long millisActualTime = System.currentTimeMillis();
            int count = 5;
            cities.clear();
            notInWordList.clear();
            String capital = randPair();
            String underscore = new String(new char[capital.length()]).replace("\0", "_");
            while (count > 0 && underscore.contains("_")) {
                guesingTries++;
                System.out.println("Guess any letter in the word, or guess whole word");
                System.out.println("Acctual lifePoints: " + count);
                System.out.println("Wrong letters, that you already used: " + notInWordList);
                System.out.println(underscore);
                String guess = sc.next().toUpperCase();
                String newUnderscore = "";

                for (int i = 0; i < capital.length(); i++) {
                    if (capital.charAt(i) == guess.charAt(0)) {
                        newUnderscore += guess.charAt(0);
                    } else if (guess.equals(capital)) {
                        newUnderscore += guess;
                    } else if (underscore.charAt(i) != '_') {
                        newUnderscore += capital.charAt(i);
                    } else {
                        newUnderscore += "_";

                    }
                }

                if (underscore.equals(newUnderscore)&& guess.length()==1) {
                    count--;
                    hangmanImage(count);
                    System.out.println("Actually life points:  " + count);
                    notInWordList.add(guess.charAt(0));
                } else {
                    underscore = newUnderscore;
                }
                if (underscore.equals(capital)) {
                    count++;

                    System.out.println("Correct!, You win!, The word was " + capital);
                    System.out.println(capital + " is the capital of " + cities);
                    System.out.println("Actually life points: " + count);
                    scoring(millisActualTime,guesingTries, capital);
                }
                if (guess.equals(capital)) {
                    count = count + 2;
                    System.out.println("Correct!, You win!, The word was " + capital);
                    System.out.println("Actually life points: " + count);
                    System.out.println(capital + " is the capital of " + cities);
                    scoring(millisActualTime,guesingTries,capital);
                }
                if (!guess.equals(capital) && guess.length() > 1) {
                    count = count - 2;
                    System.out.println("Wrong word! Your are greedy and you lose: " + 2 + " life points  ");
                    System.out.println("Actually life points:  " + count);


                }
            }




            System.out.println("If u want restart your game press: [1]");
            System.out.println("or press [2] for end");
            n = sc.nextInt();
        }
        }


    public static String randPair() {
        String capital = null;
        ArrayList<String> europeanCapitalsList = new ArrayList<>();
        String fileName = "capitals.txt";

        try (
                var fileReader = new FileReader(fileName);
                var reader = new BufferedReader(fileReader);
        ) {
            String nextLine = null;

            while ((nextLine = reader.readLine()) != null) {
                europeanCapitalsList.add(nextLine);
            }
            Random rand = new Random();

            int europeanPair = rand.nextInt(europeanCapitalsList.size());
            String pairOfWords = europeanCapitalsList.get(europeanPair);
            //System.out.println(pairOfWords);
            String[] arrOfWords = pairOfWords.split("[|]");
            capital = arrOfWords[1].replace(" ", "").toUpperCase();
            String city = arrOfWords[0].trim();
            cities.add(city);
            //System.out.println("The capital of " + city);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return capital;
    }

    public static void hangmanImage(int count) {

        if (count == 4) {
            System.out.println("Wrong guess, try again");
            System.out.println("   ____________");
            System.out.println("   |");
            System.out.println("   |");
            System.out.println("   |");
            System.out.println("   |");
            System.out.println("   |");
            System.out.println("   |");
            System.out.println("   | ");
            System.out.println("___|___");
        }
        if (count == 3) {
            System.out.println("Wrong guess, try again");
            System.out.println("   ____________");
            System.out.println("   |          _|_");
            System.out.println("   |         /   \\");
            System.out.println("   |        |     |");
            System.out.println("   |         \\_ _/");
            System.out.println("   |");
            System.out.println("   |");
            System.out.println("   |");
            System.out.println("___|___");
        }
        if (count == 2) {
            System.out.println("Wrong guess, try again");
            System.out.println("   ____________");
            System.out.println("   |          _|_");
            System.out.println("   |         /   \\");
            System.out.println("   |        |     |");
            System.out.println("   |         \\_ _/");
            System.out.println("   |           |");
            System.out.println("   |           |");
            System.out.println("   |");
            System.out.println("___|___");
        }
        if (count == 1) {
            System.out.println("Wrong guess, try again");
            System.out.println("   ____________");
            System.out.println("   |          _|_");
            System.out.println("   |         /   \\");
            System.out.println("   |        |     |");
            System.out.println("   |         \\_ _/");
            System.out.println("   |           |");
            System.out.println("   |           |");
            System.out.println("   |          / \\ ");
            System.out.println("___|___      /   \\");
        }
        if (count == 0) {
            System.out.println("GAME OVER!");
            System.out.println("   ____________");
            System.out.println("   |          _|_");
            System.out.println("   |         /   \\");
            System.out.println("   |        |     |");
            System.out.println("   |         \\_ _/");
            System.out.println("   |          _|_");
            System.out.println("   |         / | \\");
            System.out.println("   |          / \\ ");
            System.out.println("___|___      /   \\");
            System.out.println("GAME OVER! The word was ");
        }
    }
    public static void scoring(long millisActualTime, int guesingTries, String capital){
        Scanner sc = new Scanner(System.in);
        long executionTime = System.currentTimeMillis();
        long time = executionTime - millisActualTime;
        System.out.println("You guessed capital after: "+ guesingTries+ " letter and it tooks's you:  "+ TimeUnit.MILLISECONDS.toSeconds(time)+ " seconds");

        System.out.println("Podaj swoje imię: ");
        String name = sc.next();

        String fileName = "highScore.txt";
        File file = new File(fileName);

        boolean fileExists = file.exists();
        if (!fileExists) {
            try {
                fileExists = file.createNewFile();
            } catch (IOException e) {
                System.out.println("Nie udało się utworzyć pliku");
            }
        }

        if (fileExists)

            System.out.println("Plik " + fileName + " istnieje lub został utworzony");

        try (
                var fileWriter = new FileWriter(fileName);
                var writer = new BufferedWriter(fileWriter);
        ) {
            LocalDateTime now = LocalDateTime.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String guesingTime= now.format(formatter);
            writer.write("i.e " + name+" | "+guesingTime +" | "+guesingTries+" guesingTries | "+TimeUnit.MILLISECONDS.toSeconds(time)+" seconds | "+ capital);
            writer.newLine();
        } catch (IOException e) {
            System.err.println("Nie udało się zapisać pliku " + fileName);
        }
    }
}
